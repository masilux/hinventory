#!/usr/bin/perl
#Script cr�e par Julien SAFAR for H-Inventory Project
#AutoUpdate v0.2 to update your Hinventory Linux script
#Copyright (C) 2005  Julien SAFAR
#
##This program is free software; you can redistribute it and/or
##modify it under the terms of the GNU General Public License
##as published by the Free Software Foundation; either version 2
##of the License, or (at your option) any later version.
#
##This program is distributed in the hope that it will be useful,
##but WITHOUT ANY WARRANTY; without even the implied warranty of
##MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##GNU General Public License for more details.
#
##You should have received a copy of the GNU General Public License
##along with this program; if not, write to the Free Software
##Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use LWP::Simple;
use version;

#Version of autoupdate script
$autoupdate_version = "0.2";


sub check_os
 {
if (-e "hinventory_Linux.sh")
   {
   $os_version="Linux";
   $go_linux ='0';
   }
   elsif (-e "hinventory_FreeBSD.sh")
   {
   $os_version="FreeBSD";
   $go_freebsd ='1';
   }
print "$os_version script: \n";
 }
 

#Verify your local version of the script

sub localversion
{
open(VERSION,"hinventory_$os_version.sh");
@localversion = <VERSION>;
close(VERSION);
foreach $var ( @localversion )
  {
     if($var =~/^VERSION/)
     {
     @localversion= split ( /[\s,=]/, $var);
     $localversion[1] =~s/"//;
     chomp $localversion[1];
     $actualversion = $localversion[1];
     $myversion = version->new("$actualversion");
print "Your acual version is: $myversion \n";
     }
  }
}

sub lastversion
{
my $url = 'http://www.h-inventory.com/download/scripts/VERSION';
my $content = get $url;
die "Can't go to $url" unless defined $content;
chomp $content;
@recupversion = split /\s+/, $content;
if ($go_linux=='0')
    {
    @version = split /:/, $recupversion[0];
    $newversion = $version[1];
    }
elsif ($go_freebsd=='1')
   {
    @version = split /:/, $recupversion[2];
    $newversion = $version[1];
    }
$lastversion = version->new("$newversion");
print "The last version is : $lastversion \n";
}

sub update
{
   if ($myversion<$lastversion)
   {
   print "Download in progress ... \n"; 
   $download='http://www.h-inventory.com/download/scripts/hinventory'.$os_version.'-'.$newversion.'.tar.gz';
   $file = 'hinventory'.$os_version.'-'.$newversion.'.tar.gz';
   getstore($download, $file);
   system ("tar -zxvf $file");
   system ("mv hinventory.conf hinventory.confold");
   system ("cp hinventory$os_version/* .");
   system ("mv hinventory.confold hinventory.conf; rm -Rf hinventory$os_version");
   print "You updated your $os_version script successfully. Now you have the last version $lastversion ! \n";
   }
   elsif ( $ARGV[0]=~/-f$/ )
   {
    print "Download in progress ... \n"; 
    $download='http://www.h-inventory.com/download/scripts/hinventory'.$os_version.'-'.$newversion.'.tar.gz';
    $file = 'hinventory'.$os_version.'-'.$newversion.'.tar.gz';
    getstore($download, $file);
    system ("tar -zxvf $file");
    system ("mv hinventory.conf hinventory.conf.bak");
    system ("cp hinventory$os_version/* .");
    system ("rm -Rf hinventory$os_version"); 
    print "You updated your $os_version script successfully. Now you have the last version $lastversion! \n"; 
    print "You use -f option to force inventory \n";
    print "There is an important update in the hinventory.conf file \n";
    print "Do not forget to reconfigure your hinventory.conf file \n";
    }
   else
   {
   print "No Updates needed, you have the last version of the $os_version script. \n";
   }
}


if($ARGV[0]=~/-v$/)
 {
 print "Autoupdate version: $autoupdate_version \n";
 }
elsif($ARGV[0]=~/-h$/)
 {
print <<HELP;
AUTOUPDATE LINUX/FREEBSD SCRIPT
V.0.2 H-INVENTORY PROJECT         
          Options :

         -f : Force update (important update in hinventory.conf)
         -v : Show autoupdate version
         -h : Help
HELP
 }
else
 {
    &check_os;
    &localversion;
    &lastversion;
    &update;

 }
