#!/usr/bin/perl
#Soap Client in perl for Upload the xml file.
#Copyright (C) 2005  Julien SAFAR

#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


use SOAP::Lite;
use MIME::Base64 qw(encode_base64);

sub confdir 
{

   if (-e "hinventory.conf") 
   {
   $fileconf="hinventory.conf";
   }
   elsif (-e "/etc/hinventory.conf") 
   {
   $fileconf="/etc/hinventory.conf";
   } 
   elsif (-e "hinventory_Linux.conf")
   {
   $fileconf="hinventory_Linux.conf";
   }
   elsif (-e "/etc/hinventory_Linux.conf")
   {
   $fileconf="/etc/hinventory_Linux.conf";
   }
   else 
   {
   print "No file configuration found"; 
   }
}

sub infos
{
   open(CONF,$fileconf);
   @fichier = <CONF>;
   close(CONF);
      foreach $var ( @fichier )
      {
         if($var =~ /server_soap/)
         {
         @recupserver = split( /=/ , $var) ;
         chomp $recupserver[1];
         $wsdl= "$recupserver[1]";
         }
   
         if($var =~ /outputdir/)
         {
         @recuppath = split( /=/ , $var) ; 
         chomp $recuppath[1];
         @recupoutputdir = split( /#/ , $recuppath[1]) ;
         $recupoutputdir[0] =~s/[ \t]+$//;
         chomp $recupoutputdir[0];
         $outputdir= $recupoutputdir[0];
         }
      }
}

sub uploadxml
{
   open(thefile,$ARGV[0]) or die "$!";
   local $/; 
   my $file_encoded = encode_base64 (<thefile>);
   $server = SOAP::Lite -> service($wsdl);
   $server -> uploadXml($file_encoded);
}

&confdir;
&infos;
&uploadxml;
