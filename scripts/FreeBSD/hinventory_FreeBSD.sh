#!/bin/sh
set -e
#
# HInventory script for FreeBSD
# 
# Written by Thomas BRETON <thomas.breton@iscio.com>
# and David DU SERRE TELMON <david@mota-conseil.fr>
#
#Copyright (C) 2005  Thomas BRETON and David DU SERRE TELMON

#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


### Global variables
VERSION="1.2.6 for FreeBSD"
OS=`uname -s`
HOSTNAME=`hostname`
DATE="`date +%d/%m/%Y` `date +%H:%M:%S`"

##UNSET des langues
unset LANG LC_ALL LC_MESSAGES

##################################################################################
### COMMON TO ALL SCRIPTS
##################################################################################
##################################################################################
#Component writing
#4 functions 
##################################################################################
writecomment () {
  echo "<!-- $1 -->" >> $ReportFile
  if [ "$debug" = 2 ]; then
    echo "<!-- $1 -->"
  fi
  }

##################################################################################
# Sanity check, replace &, < et > with HTML codes
##################################################################################
correct_var () {
  echo "$1" | sed 's/&/\&amp;/g'  | sed 's/</\&lt;/g' | sed 's/>/\&gt;/g'
  }

writecomponent () {
  var1=`correct_var "$1"`
  var2=`correct_var "$2"`
  echo "<component>" >> $ReportFile
  echo "<type>$var1</type>" >> $ReportFile
  echo "<name>$var2</name>" >> $ReportFile
  if [ "$debug" = 2 ]; then
    echo "<component>"
    echo "<type>$var1</type>"
    echo "<name>$var2</name>"
  fi
  }

writesubcomponent () {
  var1=`correct_var "$1"`
  var2=`correct_var "$2"`
  echo "<attr><name>$var1</name><value>$var2</value></attr>" >> $ReportFile
  if [ "$debug" = 2 ]; then
    echo "<attr><name>$var1</name><value>$var2</value></attr>"
  fi
  }

writeendcomponent () {
  echo "</component>" >> $ReportFile
  if [ "$debug" = 2 ]; then
    echo "</component>"
  fi
  }

##################################################################################
## Temps d execution du script
##################################################################################

timecmd () {
  diffsec=$(($2-$1))
  heures=$(($diffsec/3600))
  minutes1=$(($diffsec-$heures*3600))
  minutes=$(($minutes1/60))
  secondes1=$(($diffsec-$minutes*60))
  secondes=$(($secondes1-$heures*3600))
  timeres="$heures heures $minutes minutes $secondes secondes"
  }

##################################################################################
## Couleur
##################################################################################
black='\E[30;47m'
red='\E[31;47m'
green='\E[32;47m'
yellow='\E[33;47m'
blue='\E[34;47m'
magenta='\E[35;47m'
cyan='\E[36;47m'
white='\E[37;47m'

# Color-echo. | Argument $1 = message | Argument $2 = color
cecho () {
  local default_msg="No message passed."
  # Doesn't really need to be a local variable.

  message=${1:-$default_msg}   # Defaults to default message.
  color=${2:-$black}           # Defaults to black, if not specified.

  #echo -e "$color" "$message" 
  echo "$message" 
  #tput sgr0                      # Reset to normal.
  }


##################################################################################
## Infos de script
##################################################################################

detect_script () {
  comment=SCRIPT
  type=script
  name=inventory
  version=$VERSION
  time=$(($2-$1))
  if [ "$debug" = 1 ]; then echo "- detect_script: $name"; fi
  if [ "$debug" = 1 ]; then echo "  * Version $version: [$time Sec] ($method)"; fi

  writecomment "$comment"
  writecomponent "$type" "$name"
  writesubcomponent "version" "$version"
  writesubcomponent "time" "$time"
  writesubcomponent "method" "$method"
  writesubcomponent "location" "$location"
  writeendcomponent
  }

##################################################################################
#Beginning of XML
##################################################################################
beginXML () {
  if [ "$debug" = 1 ]; then echo "- Initialise XLM file : $ReportFile"; fi
  rm -f $ReportFile
  touch $ReportFile
  if [ `ls -l $ReportFile | awk '{print $3}'` != `whoami` ]; then
    echo "POSSIBLE SECURITY PROBLEM. Abording script"
    exit 1
  fi
  cat << EOF > $ReportFile
<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE computer [
<!ELEMENT computer      (hostname, datetime, component*)>
<!ELEMENT component     (type,name,attr*)>
<!ELEMENT hostname      (#PCDATA)>
<!ELEMENT datetime (#PCDATA)>
<!ELEMENT type          (#PCDATA)>
<!ELEMENT name          (#PCDATA)>
<!ELEMENT attr          (name,value)>
<!ELEMENT value         (#PCDATA)>
]>
<computer>
<hostname>$HOSTNAME</hostname>
<datetime>$DATE</datetime>
EOF
  }

##################################################################################
#End of XML
##################################################################################
endXML () {
  echo "</computer>" >> $ReportFile
  }

##################################################################################
# Check arguments
##################################################################################
check_login () {
  if [ "$debug" = 1 ]; then echo "- Check login information"; fi
  fail=0
  # Check server
  if [ -z "$server" ]; then
    echo "  Server: Fail (empty)"
    fail=1
  fi
  # Check login
  if [ -z "$user" ]; then
    echo "  Username: Fail (empty)"
    fail=1
  fi
  # Check password
  if [ -z "$password" ] && [ "$method" != scp ] ; then
    echo "  Password: Fail (empty)"
    fail=1
  fi
  # Check public_key
  if [ "$method" = scp ]; then
    if [ -z "$public_key" ]; then
      echo "  Pulic key: Fail (empty)"
      fail=1
    else 
      if [ ! -f "$public_key" ]; then
        echo "  Pulic key: Fail (file $public_key not found)"
        fail=1
     fi 
   fi
  fi
  # Check mountpoint
  if [ "$method" = smb ] || [ "$method" = nfs ]; then
        if [ -z "$mountpoint" ] ; then
            echo "  Mountpoint: Fail (empty)"
            fail=1
         fi
  fi
  #failure
  if [ $fail = 1 ]; then
    echo "Error, on or more problem(s) has been found for method $method. Review your configuration."
    exit 1
  fi
  }

##################################################################################
### DIFFER BETWEEN SCRIPTS
##################################################################################

##################################################################################
# OS detection
##################################################################################
detect_os () {
  comment="OS"
  type="Operating System"
  name=`uname -s`
  version=`uname -r`

  if [ "$debug" = 1 ]; then
    if [ "$color" = 1 ]; then
      cecho "- detect_os: $comment" $green 
      cecho "  * $type: $name" $red
      cecho "  * Version: $version" $blue
    else
      echo "- detect_os: $comment"
      echo "  * $type: $name"
      echo "  * Version: $version"
    fi
  fi

  writecomment "$comment"
  writecomponent "$type" "$name"
  writesubcomponent "version" "$version"
  writeendcomponent
  }

##################################################################################
# Software detection
# Compat: FreeBSD
##################################################################################
detect_software () {
  comment=APPLICATIONS
  type=application

  writecomment "$comment"
  if [ "$debug" = 1 ]; then echo "- detect_software: $comment"; fi

  for soft in `pkg_info 2> /dev/null | awk '{ print $1 }'`; do
    pkg_desc=`pkg_info -c $soft | grep -v '^Information for' | grep -v ^$ | grep -v 'Comment:'`
    if [ "$debug" = 1 ]; then echo "   * $soft ($pkg_desc)"; fi
    writecomponent "$type" "$soft"
    writesubcomponent "commentaire" "$pkg_desc"
    writeendcomponent
  done
  }

detect_update () {
comment=UPDATES
type=updates

writecomment "$comment"
if [ -x '/usr/local/sbin/portversion' ]
then echo "Check softwares updates in progress"
	for the_update in `portversion -l "<" |cut -d " " -f1`; do
	writecomponent "$type" "$the_update"
        writeendcomponent
	done
fi
}
##################################################################################
# CPU detection 
##################################################################################
detect_cpu () {
  comment=CPU
  type=CPU

  writecomment "$comment"
  cpuname=`sysctl -n hw.model`
  nbcpu=`sysctl -n hw.ncpu`
  cpufreq=`cat /var/run/dmesg.boot | grep '^CPU: ' | sed 's/.*(//' | sed 's/)//' | sed 's/-MHz.*//'| cut -d"." -f1`

  if [ "$debug" = 1 ]; then
    echo "- detect_cpu: $comment"
    echo "  * Quantity: $nbcpu"
    echo "  * Name: $cpuname"
    echo "  * Frequency: $cpufreq"
  fi

  while [ $nbcpu -ne 0  ]; do
    nbcpu=$(($nbcpu - 1))
    writecomponent "$type" "$cpuname"
    writesubcomponent "speed" "$cpufreq"
    writeendcomponent
  done
  }

##################################################################################
#Mem detection 
##################################################################################
detect_ram () {
  comment="Memory"
  type="Physical Memory"
  name="Physical Memory"
  writecomment "$comment"

  ramsize=`sysctl -n hw.physmem`
  print_ramsize=$(($ramsize/1024/1024))

  if [ "$debug" = 1 ]; then
    print_ramsize=$(($ramsize/1024/1024))
    echo "- detect_ram: $comment"
    echo "  * Size: $print_ramsize MB"
  fi

  writecomponent "$type" "$name"
  writesubcomponent "size" "$print_ramsize"
  writeendcomponent
  }

##################################################################################
# Swap detection
##################################################################################
detect_swap () {
  comment="Memory"
  type="Audit"
  name="Virtual Memory"

  swaptotal=`swapinfo | tail -n1 |awk '{ print $2 }'`
  #swapused=`swapinfo | tail -n1 |awk '{ print $3 }'`
  #swapfree=`swapinfo | tail -n1 |awk '{ print $4 }'`
  #swapusage=`swapinfo | tail -n1 |awk '{ print $2 }'`

  print_swaptotal=$(($swaptotal/1024))
  if [ "$debug" = 1 ]; then
    echo "- detect_swap: $comment"
    echo "  * Size: $print_swaptotal MB"
  fi

  writecomment "$comment"
  writecomponent "$type" "$name"
  writesubcomponent "size" "$print_swaptotal"
  writeendcomponent
  }


##################################################################################
#Disques #TODO:traiter les bandes
##################################################################################
detect_hdd () {
  comment=HDD
  type="Hard Disk"

  writecomment "$comment"

  if [ "$debug" = 1 ]; then echo "- detect_hdd: $comment"; fi

  for disk in `sysctl -n kern.disks`; do
    name=`cat /var/run/dmesg.boot | grep -w "^$disk:" | grep "<*>" | cut -d"<" -f2 | cut -d">" -f1`
    size_tmp=`cat /var/run/dmesg.boot | grep -w "^$disk:" | grep "MB" | head -n 1`
    begin_size=`echo "$size_tmp" | sed 's/MB.*//'`
    nb_word=`echo $begin_size | wc -w`

    if [ $nb_word != 0 ]; then size=`echo $size_tmp | cut -d" " -f $nb_word` ; fi

    interface=""
    if echo "$disk" | grep ^ad > /dev/null; then interface=IDE; fi	# ATAPI disk (IDE)
    if echo "$disk" | grep ^da > /dev/null; then interface=SCSI; fi	# SCSI disk
    if echo "$disk" | grep ^md > /dev/null; then interface=SCSI; name=tape; size=unknown; fi # Tape (Memory Disk)

    if [ "$debug" = 1 ]; then echo "  * $disk: $name [$size] ($interface)"; fi

    size=`echo $size | cut -d"M" -f1`
    writecomponent "$type" "$name"
    writesubcomponent "size" "$size"
    writesubcomponent "interface" "$interface"
    writeendcomponent
  done
  }

##################################################################################
# Removable
##################################################################################
detect_removable_fnc () {
  perif=$1
  type=$2
  interface=$3
  name="`cat /var/run/dmesg.boot | grep -w "^$perif" | cut -d "<" -f2 | cut -d ">" -f1 | head -n 1`"
  writecomponent "$type" "$name"
  if [ -z "$interface" ]; then writesubcomponent "interface" "$interface"; fi
  writeendcomponent
  if [ "$debug" = 1 ]; then echo "  * $perif: $name ($type) $interface"; fi
  }

detect_removable () {
  comment="Removable"
  writecomment "$comment"

  if [ "$debug" = 1 ]; then echo "- detect_removable: $comment"; fi

  ### Removable IDE CD-Rom 
  for i in `cat /var/run/dmesg.boot | grep -w "^acd[0-9]:" | cut -d: -f 1 | sort | uniq`; do
    detect_removable_fnc "$i" "CD/DVD Drive" "IDE"
  done

  ### Removable SCSI CD-Rom 
  for i in `cat /var/run/dmesg.boot | grep -w "^cd[0-9]:" | cut -d: -f 1 | sort | uniq`; do
    detect_removable_fnc "$i" "CD/DVD Drive" "SCSI"
  done

  ### Floppy
  for i in `cat /var/run/dmesg.boot | grep -w "^fdc[0-9]:" | cut -d: -f 1 | sort | uniq`; do
    detect_removable_fnc "$i" "Floppy" ""
  done
  }

##################################################################################
#PCI detection
##################################################################################
detect_pci () {
  comment="Cards"
  writecomment "$comment"

  if [ "$debug" = 1 ]; then echo "- detect_pci: $comment"; fi

  pciconf=`pciconf -lv`
  perif=`echo "$pciconf" | grep [[:alnum:]]@[[:alnum:]:[:alnum:]:[:alnum:]:] | cut -d@ -f 1`
  for i in $perif; do
    name=`echo "$pciconf" | grep -w ^$i -A 4 | grep -w device   | cut -d\' -f 2 | sed 's/\?\?\?\ //'`
    mark=`echo "$pciconf" | grep -w ^$i -A 4 | grep -w vendor   | cut -d\' -f 2`
    class=`echo "$pciconf" | grep -w ^$i -A 4 | grep -w class | cut -d\= -f 2 | tail -n 1 | sed 's/.* //'`
    subclass=`echo "$pciconf" | grep -w ^$i -A 4 | grep -w subclass | cut -d\= -f 2 | sed 's/.* //'`
    type="$class $subclass"

    if [ "$class" != "network" ]; then
      if [ "$subclass" = "SCSI" ]; then type="SCSI Controller"; fi
      if [ "$subclass" = "ATA" ]; then type="IDE Controller"; fi
      if [ "$subclass" = "USB" ]; then type="USB Controller"; fi
      if [ "$subclass" = "VGA" ]; then type="Graphic Card"; fi

      if [ "$debug" = 1 ]; then echo "  * $i: $name - $mark ($type)"; fi

      writecomponent "$type" "$name"
      writesubcomponent "manufacturer" "$mark"
      writeendcomponent
  fi
done
}

##################################################################################
#Network detection
##################################################################################
detect_network () {
  comment=NETWORK
  writecomment "$comment"

  type="Network Adapter"
  pciconf=`pciconf -lv`
  #pciconf=`cat pciconf`

  if [ "$debug" = 1 ]; then echo "- detect_network: $comment"; fi

  for i in `ifconfig -l`; do
    if ifconfig $i | grep 'status:' > /dev/null; then
      hwname=$i
      name="Not found"
      mark=$name
      # Guest hardware NIC name (sk card are name skc in pciconf)
      if ! echo "$pciconf" | grep ^$i > /dev/null; then
        int_name=`echo $i | sed 's/[0-9]//'`
        int_nb=`echo $i | sed s/$int_name//`
        hwname=""
        for y in a b c d e f g h i k l m n o p q r s t u v w x y z; do
          if echo "$pciconf" | grep -w ^${int_name}${y}${int_nb} > /dev/null; then
            hwname=${int_name}${y}${int_nb};
          fi
        done
      fi

      if [ -n "$hwname" ]; then
        name=`echo "$pciconf" | grep -w ^$hwname -A 4 | grep -w device   | cut -d\' -f 2 | sed 's/\?\?\?\ //'`
        mark=`echo "$pciconf" | grep -w ^$hwname -A 4 | grep -w vendor   | cut -d\' -f 2`
      fi
      ip=`ifconfig $i | grep -w inet | cut -f2 -d' ' | head -n 1`
      mac=`ifconfig $i | grep ether | cut -f2 -d' '`
      if [ -z "`ifconfig $i | grep netmask`" ]; then
        mask="0.0.0.0"
      else
      	o1=`ifconfig $i | grep netmask | cut -d" " -f4 | cut -d"x" -f2 | cut -b1-2 | tr [a-z] [A-Z]`
      	o2=`ifconfig $i | grep netmask | cut -d" " -f4 | cut -d"x" -f2 | cut -b3-4 | tr [a-z] [A-Z]`
      	o3=`ifconfig $i | grep netmask | cut -d" " -f4 | cut -d"x" -f2 | cut -b5-6 | tr [a-z] [A-Z]`
      	o4=`ifconfig $i | grep netmask | cut -d" " -f4 | cut -d"x" -f2 | cut -b7-8 | tr [a-z] [A-Z]`
      	o1=`echo "ibase=16; $o1" | bc`
      	o2=`echo "ibase=16; $o2" | bc`
      	o3=`echo "ibase=16; $o3" | bc`
      	o4=`echo "ibase=16; $o4" | bc`
      	mask=`echo "$o1.$o2.$o3.$o4"`
      fi

      if [ -z "$ip" ]; then ip="0.0.0.0"; fi
      if [ "$debug" = 1 ]; then echo "  * $i: $name - $mark - MAC: $mac IP: $ip MASK: $mask"; fi

      writecomponent "$type" "$name"
      writesubcomponent "manufacturer" "$mark"
      writesubcomponent "mac" "$mac"
      writesubcomponent "ip" "$ip"
      writesubcomponent "subnetmask" "$mask"
      writeendcomponent
    fi
  done
  }

##################################################################################
#Disk Audit
##################################################################################
disk_audit () {
  comment=DISK
  if [ "$debug" = 1 ]; then echo "- disk_audit: $comment"; fi

  nb=`df -lPhm | wc -l`
  nbdisk=$(($nb-1))

  type="Audit"

  for part in `df -lPhm | tail -n $nbdisk | awk '{ printf "%s;%s;%s;%s;%s;%s\n", $1, $2, $3, $4, $5, $6 }'`; do
    name=`echo $part | cut -d";" -f1`
    size=`echo $part | cut -d";" -f2`
    used=`echo $part | cut -d";" -f3`
    available=`echo $part | cut -d";" -f4`
    percent=`echo $part | cut -d";" -f5`
    mount=`echo $part | cut -d";" -f6`

    writecomponent "$type" "Partition $name"

    writesubcomponent "size" "$size"
    writesubcomponent "freespace" "$available"
    writesubcomponent "used" "$used"
    writesubcomponent "percent" "$percent"
    writesubcomponent "mountpoint" "$mount"
    writeendcomponent
  done
  }
	      


##################################################################################
#Upload XML file
##################################################################################

uploadXML () {
  case $method in
    local) exit 0 ;;
    ### Upload SCP
    scp)
      if [ "$debug" = 1 ]; then echo "- Starting upload on scp://${user}@${server}/${remote_path}"; fi
      scp -i $public_key $ReportFile $user@$server:$remote_path
      if [ $? = 0 ]; then
        echo "  * Report succesfully uploaded on $server."
      else
        echo "  * Unable to upload report file on $server."
      fi
    ;;
  ### Upload HTTP
  http)
    if [ -x '/usr/local/bin/curl' ]; then 
    echo "- Starting upload on http://${user}@${server}${remote_path}"; 
    /usr/local/bin/curl -F file1=@$ReportFile -u ${user}:${password} -F SubBtn=OK ${server}${remote_path};
    else	
     echo "To use HTTP upload , install CURL before."
    fi 
    ;;
  ### Upload FTP
  ftp)
    if [ "$debug" = 1 ]; then echo "- Starting upload on ftp://${user}@${server}/${remote_path}"; fi
      ftp -i -n $server > /dev/null 2>&1 << EOF
user ${user} ${password}
binary
cd $remote_path
lcd `dirname $ReportFile`
put `basename $ReportFile`
quit
EOF
    if [ $? = 0 ]; then
      echo "  * Report succesfully uploaded on $server."
    else
      echo "  * Unable to upload report file on $server."
    fi
  ;;
  ### Upload SOAP 
  soap)
  if [ -f "hisoap_client.pl" ]
        then ./hisoap_client.pl $ReportFile
  else if [ -f "/bin/hisoap_client.pl" ]
        then /usr/local/sbin/hisoap_client.pl $ReportFile
  fi
  fi
  ;;
  ### Upload SMB
  smb)
    if [ "$debug" = 1 ]; then echo "- Starting upload on smb://${user}@${server}/${remote_path}"; fi
    if [ -e $mountpoint ]
       then
       echo
       else
       mkdir $mountpoint
       fi
       mount -t smbfs -o username=$user,password=$password //$server/$remote_path $mountpoint
       cp $ReportFile $mountpoint
    if [ $? = 0 ]; then
      echo "  * Report succesfully uploaded on $server."
    else
      echo "  * Unable to upload report file on $server."
    fi
       umount $mountpoint
    ;;
   ### Upload NFS
   nfs)
     if [ "$debug" = 1 ]; then echo "- Starting upload on nfs://${user}@${server}:${remote_path}"; fi
     if [ -e $mountpoint ]; then
       echo
     else
       mkdir $mountpoint
     fi
     mount -t nfs $server:$remote_path $mountpoint
     cp $ReportFile $mountpoint
     if [ $? = 0 ]; then
       echo "  * Report succesfully uploaded on $server."
     else
       echo "  * Unable to upload report file on $server."
     fi
     umount $mountpoint
     ;;
  esac
  # Remove report file
  #rm -f $ReportFile
  }

# From FreeBSD rc.subr
checkyesno () {
  eval _value=\$${1}
  case $_value in
    #       "yes", "true", "on", or "1"
    [Yy][Ee][Ss]|[Tt][Rr][Uu][Ee]|[Oo][Nn]|1)
      return 0
      ;;
    #       "no", "false", "off", or "0"
    [Nn][Oo]|[Ff][Aa][Ll][Ss][Ee]|[Oo][Ff][Ff]|0)
      return 1
      ;;
    *)
      return 1
      ;;
  esac
  }

##################################################################################
# MAIN
##################################################################################

blnConfFile="false"
if [ -f "/usr/local/etc/hinventory.conf" ]; then
  ConfFile="/usr/local/etc/hinventory.conf"
  blnConfFile="true"
else
  if [ -f "/usr/local/etc/hinventory_FreeBSD.conf" ]; then
    ConfFile="/usr/local/etc/hinventory_FreeBSD.conf"
    blnConfFile="true"
  else
    if [ -f `dirname $0`/hinventory.conf ]; then
      ConfFile=`dirname $0`/hinventory.conf
      blnConfFile="true"
    else
      if [ -f `dirname $0`/hinventory_FreeBSD.conf ]; then
        ConfFile=`dirname $0`/hinventory_FreeBSD.conf
        blnConfFile="true"
      fi
    fi
  fi
fi

if ! $blnConfFile ; then
  echo "Fatal error, configuration file is missing."
  exit 1
else

  # Checking conf
  #. `dirname` $ConfFile

  . $ConfFile

  # Test report folder
  if [ -z "$outputdir" ] && [ -d "/var/spool/hinventory" ]; then
    if [ "$debug" = 1  ] || [ "$debug" = 2  ]; then
      echo "INFO: Report will be generate in /var/spool/hinventory folder."
    fi
    ReportFile=/var/spool/hinventory/$HOSTNAME-`date +%m-%d-%y-%H%M%s`.xml
  else
    if [ -n "$outputdir" ]; then
      if [ ! -d "$outputdir" ]; then
        if [ "$debug" = 1  ] || [ "$debug" = 2  ]; then
          echo "WARNING: Output folder $outputdir doesn't exist."
          echo "         Report will be generate in current directory."
        fi
        ReportFile=$HOSTNAME-`date +%m-%d-%y-%H%M%s`.xml
      else
        if [ "$debug" = 1  ] || [ "$debug" = 2  ]; then
          echo "INFO: Report will be generate in $outputdir folder."
        fi
        ReportFile=$outputdir/$HOSTNAME-`date +%m-%d-%y-%H%M%s`.xml
      fi
    fi
  fi

  case $debug in
    0) ;;
    1) 
      echo "- Parsing configuration file: $ConfFile" 
      echo "  * debug=$debug"
      ;;
    xml) ;;
    *) echo "Fatal error, debug mode not supported, choose : debug=[0][1][2]"; exit 1;;
  esac

  if [ -z "$remote_path" ]; then
    remote_path=.
  fi

  if [ "$debug" = 1 ]; then
    echo "  * method=$method"
    echo "  * server=$server"
    echo "  * remote_path=$remote_path"
    echo "  * mountpoint=$mountpoint"
    echo "  * user=$user"
    echo "  * password=[XXX]"
    echo
  fi

  case $method in
    ftp)
      check_login
      ;;
    http)
      check_login
      ;;
    local) 
      ;;
    smb)
      check_login
      ;;
    nfs)
      check_login
      ;;
    scp)
      check_login
      ;;
     soap)
      ;;

    *)
       echo "Fatal error, method not supported, choose : ftp, http, local, smb, nfs or scp"
       exit 1
       ;;
  esac
fi

timedeb=`date "+%s"`
beginXML

detect_cpu
detect_ram
detect_hdd
detect_removable
detect_pci
detect_network
detect_os
if checkyesno test_software; then detect_software; fi
if checkyesno test_audit; then disk_audit; detect_swap; fi
if checkyesno test_update; then detect_update; fi

timefin=`date "+%s"`
detect_script $timedeb $timefin
endXML
timecmd $timedeb $timefin
if [ "$debug" = 1 ]; then
        echo "- Temps d execution:"
        echo $timeres
fi
uploadXML

exit 0
