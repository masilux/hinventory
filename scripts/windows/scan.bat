@echo off
REM Scanning ip addresses defined in computer.txt..
setlocal
echo Scanning :

for /f %%a in (computer.txt) do (set client=%%a
call :pong)
goto eof

:pong
echo %client%...
ping -n 1 %client% >nul
if not %errorlevel% == 1 goto sp
echo [Fail]
echo.
goto eof

:sp
cscript hinventoryWindows.vbs /v /t %client%
echo.

:eof