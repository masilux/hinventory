'==========================================================================
'
' NAME: inventaire.vbs
'
' AUTHOR: Aurélien Roux , iscio
' DATE  : 21/07/06
'
' COMMENT: Permet la création d'un fichier XML contenant la configuration machine
' COMMENT: Generate an XML inventory file about computer hardware and software 
'
'==========================================================================
On Error Resume Next

' Temps de référence
Datedeb = Now
'#####################
' Constantes
Const VERSION = "1.4.9"
Const BDATE = "31/03/07"
Const OS = "Windows"
Const XSLPAGE = "hinventory.xsl"
Const TEMPORARYFOLDER = 2

' Variable à déclarer pour la prise en charge du fichier de configuration
Dim server, user, password, server_http, user_http, password_http, remote_path_http, remote_path, location

' Variable script
Dim pathsmb, ConfFilePath
' On récupère l'emplacement du script, emplacement où se situe également le fichier de configuration
ConfFilePath = GetScriptPath()
pathsmb = "./"

' Si B_App égale VRAI, alors on liste les applis
B_App = False
' Si B_Audit égale VRAI, alors on active l'audit
B_Audit = False
' Mode Verbose Off
B_verbose = False
' Mode VeryVerbose Off
B_vverbose = False
' Liens XSL off
B_xsl = False
' periphérique USB
B_usb = False
' temps d'execution du script
B_perf = False
' variable information sur la méthode d'upload du fichier
str_Method = "local"
'#####################

' Procédure de récupération du chemin du script
' Procédure d'écriture dans le fichier XML
' et informations à propos des barettes de RAM

Function GetScriptPath()
	set i = CreateObject("Scripting.FileSystemObject")
	GetScriptPath = i.GetParentFolderName(wscript.ScriptFullName)
	Set i = Nothing
End Function

Sub WriteXMLTag (file, tag)
	file.writeLine("<"&tag&">")
End Sub

Function EncodeUTF8(s)
  dim i,c
  i = 1
  do while i < len(s)
    c = asc(mid(s,i,1))
    if c >= &H80 then
      s = left(s,i-1) + chr(&HC2 + ((c and &H40) / &H40)) + chr(c and &HBF) + mid(s,i+1)
      i = i + 1
    end if
    i = i + 1
  loop
  EncodeUTF8 = s 
End Function

' Remplace les \ par des / 
Function ASReplace(s)
  ASReplace = Replace(s,"\","/")
End Function

' Réencode certains caractères en code HTML
Function EncodeChar(chaine)
	If chaine <> "" Then
		EncodeChar = EncodeUTF8(chaine)
		EncodeChar = Replace(chaine,"&","&amp;")
		EncodeChar = Replace(EncodeChar,"<","&lt;") 
		EncodeChar = Replace(EncodeChar,">","&gt;")
	End If
End Function

Sub WriteXMLTagContent (file, tag, value)
	If IsNull(value) Then value = "N/A"
	If IsEmpty(value) Then value = "N/A"
	str = "<"&tag&">"&EncodeChar(value)&"</"&tag&">"
	If B_vverbose = True Then WScript.Echo str
	file.writeLine(str)
End Sub

Sub WriteXMLAttrTag (file, tag, value)
	If IsNull(value) Then value = "N/A"
	If IsEmpty(value) Then value = "N/A"
	str = "<attr><name>"&tag&"</name><value>"&EncodeChar(value)&"</value></attr>"
	If B_vverbose = True Then WScript.Echo str
	file.writeLine(str)
End Sub

Function oGetMo (octet)
	if octet <> "" Then oGetMo = round(octet/(1024*1024),0)
End Function

Function koGetMo (koctet)
	if koctet <> "" Then koGetMo = round(koctet/1024,0)
End Function

Function BatType(typebatterie)
	Select Case typebatterie
		Case 1
		BatType = "Other"
		Case 2
		BatType = "Unknown"
		Case 3
		BatType = "Lead Acid"
		Case 4
		BatType = "Nickel Cadmium"
		Case 5
		BatType = "Nickel Metal Hydride"
		Case 6
		BatType = "Lithium-ion"
		Case 7
		BatType = "Zinc air"
		Case 8
		BatType = "Lithium Polymer"
	End select
End Function
		
Function GetMemoryType(typememoire)
	Select Case typememoire
		Case 0
		GetMemoryType = "Unknown"
		Case 1
		GetMemoryType = "Other"
		Case 2
		GetMemoryType = "DRAM"
		Case 3
		GetMemoryType = "Synchronous DRAM"
		Case 4
		GetMemoryType = "Cache DRAM"
		Case 5
		GetMemoryType = "EDO"
		Case 6
		GetMemoryType = "EDRAM"
		Case 7
		GetMemoryType = "VRAM"
		Case 8
		GetMemoryType = "SRAM"
		Case 9
		GetMemoryType = "RAM"
		Case 10
		GetMemoryType = "ROM"
		Case 11
		GetMemoryType = "Flash"
		Case 12
		GetMemoryType = "EEPROM"
		Case 13
		GetMemoryType = "FEPROM"
		Case 14
		GetMemoryType = "EPROM"
		Case 15
		GetMemoryType = "CDRAM"
		Case 16
		GetMemoryType = "3DRAM"
		Case 17
		GetMemoryType = "SDRAM"
		Case 18
		GetMemoryType = "SGRAM"
		Case 19
		GetMemoryType = "RDRAM"
		Case 20
		GetMemoryType = "DDR"
	End select
End Function

Function GetFormFactor(formememoire)
	Select Case formememoire
		Case 0
		GetFormFactor = "Unknown"
		Case 1
		GetFormFactor = "Other"
		Case 2
		GetFormFactor = "SIP"
		case 3
		GetFormFactor = "DIP"
		case 4
		GetFormFactor = "ZIP"
		case 5
		GetFormFactor = "SOJ"
		case 6
		GetFormFactor = "Proprietary"
		case 7
		GetFormFactor = "SIMM"
		case 8
		GetFormFactor = "DIMM"
		case 9
		GetFormFactor = "TSOP"
		case 10
		GetFormFactor = "PGA"
		case 11
		GetFormFactor = "RIMM"
		case 12
		GetFormFactor = "SODIMM"
		case 13
		GetFormFactor = "SRIMM"
		case 14
		GetFormFactor = "SMD"
		case 15
		GetFormFactor = "SSMP"
		case 16
		GetFormFactor = "QFP"
		case 17
		GetFormFactor = "TQFP"
		case 18
		GetFormFactor = "SOIC"
		case 19
		GetFormFactor = "LCC"
		case 20
		GetFormFactor = "PLCC"
		case 21
		GetFormFactor = "BGA"
		case 22
		GetFormFactor = "FPBGA"
		case 23
		GetFormFactor = "LGA"
	End select
End Function

' Conversion date WMI
Function WMIDateStringToDate(IntervalFormat)
	Dim sYear, sMonth, sDay, sHour, sMinutes, sSeconds
	sYear = mid(IntervalFormat, 1, 4)
	sMonth = mid(IntervalFormat, 5, 2)
	sDay = mid(IntervalFormat, 7, 2)
	sHour = mid(IntervalFormat, 9, 2)
	sMinutes = mid(IntervalFormat, 11, 2)
	sSeconds = mid(IntervalFormat, 13, 2)

	' Returning format yyyy-mm-dd hh:mm:ss
  	WMIDateStringToDate = sYear & "-" & sMonth & "-" & sDay & " " _
               & sHour & ":" & sMinutes & ":" & sSeconds
End Function


' ProductKey, trouvé dans un de mes mails ;)
Function ProductKey()
	Rem lookup digital product id from registry
	Set WshShell = WScript.CreateObject("WScript.Shell")
	HexBuf = WshShell.RegRead("HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\DigitalProductId")
	For l = lBound(HexBuf) to uBound(HexBuf)
		tmp=tmp & " "& Hex (HexBuf(l))
	next
	
	StartOffset = 52
	EndOffset =67
	Dim Digits (24)
	
	Digits (0) = "B" : Digits (1) = "C": Digits (2) = "D": Digits (3) = "F":
	Digits (4) = "G": Digits (5) = "H": Digits (6) = "J": Digits (7) = "K":
	Digits (8) = "M": Digits (9) = "P": Digits (10) = "Q": Digits (11) = "R":
	Digits (12) = "T": Digits (13) = "V": Digits (14) = "W": Digits (15) = "X":
	Digits (16) = "Y": Digits (17) = "2": Digits (18) = "3": Digits (19) = "4":
	Digits (20) = "6" : Digits (21) = "7" : Digits (22) = "8": Digits (23) = "9"
	
	dLen = 29
	sLen = 15
	Dim HexDigitalPID (15)
	Dim Des (30)
	For i = StartOffset to EndOffset
		HexDigitalPID (i-StartOffset) = HexBuf(i)
		tmp2=tmp2 & " "& Hex (HexDigitalPID(i-StartOffset))
	next
	KEYSTRING =""
	for i=dLen-1 to 0 step -1
		if ((i + 1) mod 6) = 0 then
			Des (i) = "-"
			KEYSTRING =KEYSTRING & "-"
		else
			HN = 0
			    For N = (sLen -1) to 0 step -1
			      Value = ( (HN *2^8 ) Or HexDigitalPID (N))
			       HexDigitalPID (N) = Value \ 24
			       HN    = (Value mod 24)
			
			    next
			
			Des(i) = Digits(HN)
			KEYSTRING =KEYSTRING & Digits(HN)
		end if
	next
	KEYSTRING2 = StrReverse (KEYSTRING)
	ProductKey = KEYSTRING2
End Function

' Test si l'adresse MAC est une MAC Virtuel
Function testVirtualMAC(MAC)
    '		PPP									vmware						VirtualPC
	If MAC = "00:53:45:00:00:00" Or Left(MAC,8)= "00:50:56" Or Left(MAC,8)= "00:03:FF" Then
		testVirtualMAC=True
	Else
		testVirtualMAC=False
	End If
End Function

' Procedure de chargement du fichier de configuration
Sub LoadConfig()
	Const ForReading = 1
	Dim fso, MyFile, b_fileexist
	b_fileexist = false
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	If (fso.FileExists(ConfFilePath & "\hinventory_windows.conf")) Then
		Set MyFile = fso.OpenTextFile(ConfFilePath & "\hinventory_windows.conf", ForReading)
		b_fileexist = true
	Elseif (fso.FileExists(ConfFilePath & "\hinventory.conf")) Then
		Set MyFile = fso.OpenTextFile(ConfFilePath & "\hinventory.conf", ForReading)
		b_fileexist = true
	End If
	If b_fileexist = True then
		Do While MyFile.AtEndOfStream  <> True
			Line = MyFile.ReadLine
    		If left(Line,1) <> "#" And Line <> "" Then
    			Options = Split(Line, "=")
    			Select Case Options(0)
    				Case "debug"
    					' a retravailler...
						If Options(1)=0 Then B_verbose = False
						If Options(1)=1 Then B_verbose = True
						If Options(1)=3 Then B_vverbose = True
					Case "method"
						str_method = Options(1)
					Case "test_software"
						If Options(1)="yes" Then B_App = True
					Case "test_audit"
						If Options(1)="yes" Then B_Audit = True
					Case "server"
						server = Options(1)
					Case "location"
						location = Options(1)
					Case "user"
						user = Options(1)
					Case "password"
						password = Options(1)
					Case "remote_path"
						remote_path = Options(1)
					Case "server_http"
						server_http = Options(1)
					Case "user_http"
						user_http = Options(1)
					Case "password_http"
						password_http = Options(1)
					Case "remote_path_http"
						' J'utilise Mid pour éviter un problème dû à la présence de plusieurs "="
						remote_path_http = Mid(Line, Len("remote_path_http")+2)
				End Select
    		End If
		Loop
	End if
End Sub
' Chargement du fichier de config
' Par defaut, on charge le fichier de configuration...
LoadConfig()

' Routine de gestion des arguments...
' Creer un booléen correspondant à l'action ou créé des variables
i = 0
if Wscript.arguments.count>0 then
   For Each com In WScript.Arguments
   		' n'ajoute pas les applications au XML
   		If com = "/na" Or com = "/NA" then
   			B_App = False
   		End If
   		' Gère la sortie fichier
   		If com = "/f" Or com = "/F" then
   			out = WScript.Arguments.Item(i+1)
   			str_Method = "Local"
   		End If
   		' Gère la sortie 
   		If com = "/v" Or com = "/V" Then
   			B_verbose = True
   		End If
   		' Gère la sortie complete
   		If com = "/vv" Or com = "/VV" then
   			B_vverbose = True
   		End If
   		' Gère la machine cible
   		If com = "/t" Or com = "/T" Then
   			target = WScript.Arguments.Item(i+1)
   		End If
   		' Indique dans le fichier XML un fichier XLT
   		If com = "/s" Or com = "/S" then
   			B_xsl = True
   		End If
   		' Permet d'activer la détection des périphériques USB
   		If com = "/ud" Or com = "/UD" then
   			B_usb = True
   		End If
   		' Permet le calcul du temps d'exécution
   		If com = "/p" Or com = "/P" then
   			B_perf = True
   		End If
   		' Affiche l'aide
   		If com = "/?" Or com = "/H" then
   			WScript.Echo "HInventory.vbs :" & VbCrLf & "Version : " & VERSION & " du " & BDATE & VbCrLf
   			WScript.Echo "Provide inventory for your computer hardware and software as an XML file."
   			WScript.Echo "Part of the  H-Inventory tools." 
   			WScript.Echo "Site links : http://www.h-inventory.com" & VbCrLf
   			WScript.Echo "cscript inventory.vbs [/na|/NA] [/f|/F out.xml] [/v|/V] [/vv|/VV] [/t|/T TargetComputer] [/s|/S] [/ud|/UD]" & VbCrLf
   			WScript.Echo "/na or /NA : Display Application informations in the XML file."
   			WScript.Echo "/f  or /F  : Specify an XML file other than the default file."
   			WScript.Echo "/v  or /V  : Display informations about processing."
   			WScript.Echo "/vv or /VV : Display detailled informations about processing."
   			WScript.Echo "/t  or /T  : Specify the target computer by IP, hostname or computer name (default is localhost)."
   			WScript.Echo "/S  or /S  : Add a link to the XSL default page."
   			WScript.Echo "/ud or /UD : Add the list of USB peripheral in the XML file."
   			WScript.quit(0)
   		End If
		i = i + 1
   Next
end If
' Fin de la gestion des arguments...

' Initialisation
if target = "" Then target = "."
Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & target & "\root\cimv2")
Set fso = CreateObject("scripting.filesystemobject")

' Hostname
Set colCN = objWMIService.ExecQuery ("Select Caption from Win32_ComputerSystem")
For each objCN in colCN
	Hostname = LCase(objCN.Caption)
Next

' Nombre aléatoire ajouter au nom du fichier.
Dim aleaname,xmlfilename
randomize
aleaname = Int(100000*Rnd()+1000000)
xmlfilename = Hostname & "-" & aleaname & ".xml"

' Initialisation FTP si nécessaire...
If str_method = "ftp" Then
	' On crée un fichier de commande ftp temporaire dans le dossier TEMP de l'utilisateur
	Set TempFolder = fso.GetSpecialFolder(TemporaryFolder)
	str_TempFolderName = tempfolder.path
	str_TempFileName = fso.GetTempName  
	Set tempfile = tempfolder.CreateTextFile(str_TempFileName,true,false)

	TempFile.WriteLine "open " & server
	TempFile.WriteLine user
	TempFile.WriteLine password
	TempFile.WriteLine "put " & str_TempFolderName & "\" & xmlfilename
	TempFile.WriteLine "bye"
	TempFile.close
	' On génère le XML dans le repertoire TEMP
	out = str_TempFolderName & "\" & xmlfilename
End If

' Initialisation HTTP si nécessaire...
If str_method = "http" Then
	' On crée le fichier XML dans le dossier TEMP de l'utilisateur
	Set TempFolder = fso.GetSpecialFolder(TemporaryFolder)
	str_TempFolderName = tempfolder.path
	out = str_TempFolderName & "\" & xmlfilename
End If

' Initialisation SMB si nécessaire...
If str_method = "smb" Then
	' On formate le path smb
	pathsmb = "\\" & server & "\" & remote_path & "\"
End If 

' Création du fichier XML
If out = "" Then out = pathsmb & xmlfilename
If B_verbose = True or B_vverbose = True Then
	WScript.Echo "Inventoring machine " & Hostname & "..."
	WScript.Echo "out file: " & out
	WScript.Echo "Processing :"
End If
Set file = fso.createtextfile(out,true,false)

' Déclaration fichier XML
If B_verbose = True Or B_vverbose = True Then WScript.Echo "XML header & DTD..."
WriteXMLTag file,"?xml version=""1.0"" encoding=""UTF-8"" ?"
If B_xsl = True Then WriteXMLTag file,"?xml-stylesheet href="""&XSLPAGE&""" type=""text/xsl"" ?"

' DTD
file.writeLine("<!DOCTYPE computer [")
WriteXMLTag file,"!ELEMENT computer	(hostname, datetime, component*)"
WriteXMLTag file,"!ELEMENT component	(type,name,attr*)"
WriteXMLTag file,"!ELEMENT hostname	(#PCDATA)"
WriteXMLTag file,"!ELEMENT datetime	(#PCDATA)"
WriteXMLTag file,"!ELEMENT type		(#PCDATA)"
WriteXMLTag file,"!ELEMENT name		(#PCDATA)"
WriteXMLTag file,"!ELEMENT attr		(name,value)"
WriteXMLTag file,"!ELEMENT value		(#PCDATA)"
file.writeLine("]>")

WriteXMLTag file,"computer"

' Hostname
WriteXMLTagContent file, "hostname", Hostname

' Date et heure
WriteXMLTagContent file, "datetime", Date & " " & Time

' Section Modèle
WriteXMLTag file,"!-- Model --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "Model..."

' Récupération informations modèle
Set colCS = objWMIService.ExecQuery ("Select * from Win32_ComputerSystem")
For each objCS in colCS
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","Model"
    WriteXMLTagContent file,"name","Model"
    WriteXMLAttrTag file,"Manufacturer",objCS.Manufacturer
    WriteXMLAttrTag file,"Model",objCS.Model
    WriteXMLAttrTag file,"SystemType",objCS.SystemType
    WriteXMLTag file,"/component"
Next
Set colCS = Nothing

'Fermeture section modèle

' Section BIOS
WriteXMLTag file,"!-- BIOS --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "Bios..."

' Récupération BIOS
Set colBios = objWMIService.ExecQuery ("Select * from Win32_BIOS")
For each objBios in colBios
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","BIOS"
    WriteXMLTagContent file,"name",objBios.Caption
    WriteXMLAttrTag file,"manufacturer",objBios.Manufacturer
    WriteXMLAttrTag file,"SerialNumber",objBios.SerialNumber
    WriteXMLTag file,"/component"
Next
Set colBios = Nothing

'Fermeture section BIOS

' Section Batterie
WriteXMLTag file,"!-- Battery --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "Battery..."

' Récupération Informations OS
Set colBat = objWMIService.ExecQuery ("Select * from Win32_Battery")
For Each objBat in colBat 
    WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","Battery"
    WriteXMLTagContent file,"name",objBat.DeviceID
    WriteXMLAttrTag file,"description",objBat.Description
    WriteXMLAttrTag file,"Chemistry",BatType(objBat.Chemistry) 
    WriteXMLTag file,"/component"
Next
Set colBat = Nothing

'Fermeture Batterie

' Section processeurs
WriteXMLTag file,"!-- CPU --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "CPU..."

' Récupération processeur physique
Set colCS = objWMIService.ExecQuery ("Select * from Win32_ComputerSystem")
For each objCS in colCS
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","PhysicalCPU"
    WriteXMLTagContent file,"name","PhysicalCPU"
    WriteXMLAttrTag file,"NumberOfProcessors",objCS.NumberOfProcessors
    WriteXMLTag file,"/component"
Next
Set colPr = Nothing

' Récupération processeurs
Set colPr = objWMIService.ExecQuery ("Select * from Win32_Processor")
For each objPr in colPr
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","CPU"
    WriteXMLTagContent file,"name",objPr.Name
    WriteXMLAttrTag file,"manufacturer",objPr.Manufacturer
    WriteXMLAttrTag file,"description",objPr.Description
    WriteXMLAttrTag file,"speed",objPr.CurrentClockSpeed
    WriteXMLAttrTag file,"datawidth",objPr.DataWidth
    WriteXMLTag file,"/component"
Next
Set colPr = Nothing

'Fermeture section processeurs

' Section memoire
WriteXMLTag file,"!-- Memory --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "Memory..."
intTotalMemory = 0
' Récupération taille memoire
 Set colMemoire = objWMIService.ExecQuery ("Select * from Win32_LogicalMemoryConfiguration")
 For each objMemoire in colMemoire
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","Logical Memory"
    WriteXMLTagContent file,"name","Logical Memory"
    intTotalMemory = koGetMo(objMemoire.TotalPhysicalMemory)
    WriteXMLAttrTag file, "size", intTotalMemory
    WriteXMLTag file,"/component"
 Next
Set colMemoire = Nothing

' Récupération barettes memoires
' Si les informations sont vides, on réaffiche la mémoire totale
Set colbarettes = objWMIService.ExecQuery ("Select * from Win32_PhysicalMemory")
if colbarettes.count = 0 Then
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","Physical Memory"
    WriteXMLTagContent file,"name","Total Memory"
    WriteXMLAttrTag file,"size",intTotalMemory
    WriteXMLTag file,"/component"
Else
	For each objbarette in colbarettes
		WriteXMLTag file,"component"
    	WriteXMLTagContent file,"type","Physical Memory"
    	WriteXMLTagContent file,"name",objbarette.Name
    	WriteXMLAttrTag file,"size",oGetMo(objbarette.Capacity)
    	WriteXMLAttrTag file,"type",GetMemoryType(objbarette.MemoryType)
    	WriteXMLAttrTag file,"form",GetFormFactor(objbarette.FormFactor)
    	WriteXMLAttrTag file,"bank",objbarette.BankLabel
    	WriteXMLTag file,"/component"
	Next
End If
Set colbarettes = Nothing

'Fermeture section memoire

' Section Disques
WriteXMLTag file,"!-- HDD --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "HDD..."

' Récupération disques
Set colHD = objWMIService.ExecQuery ("Select * from Win32_DiskDrive")
For each objHD in colHD
	Set objHDb = objWMIService.Get("Win32_PhysicalMedia.Tag='"&objHD.DeviceID&"'")
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","Hard Disk"
    WriteXMLTagContent file,"name",objHD.Caption
    WriteXMLAttrTag file,"manufacturer",objHD.Manufacturer
    WriteXMLAttrTag file,"size",oGetMo(objHD.Size)
    WriteXMLAttrTag file,"interface",objHD.InterfaceType
    WriteXMLAttrTag file,"number",objHDb.SerialNumber
    WriteXMLTag file,"/component"
Next
Set colHD = Nothing

'Fermeture section disques

' Section Cartes
WriteXMLTag file,"!-- Cards --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "Cards..."

' Récupération Réseau
Set colCR = objWMIService.ExecQuery ("Select * from Win32_NetworkAdapterConfiguration where IPEnabled = True")
For each objCR in colCR
    If testVirtualMAC(objCR.MACAddress) = False Then
    	' On entre ici uniquement si la carte n'est pas considéré comme virtuelle
	    WriteXMLTag file,"component"
		WriteXMLTagContent file,"type", "Network Adapter"
	    WriteXMLTagContent file,"name",objCR.Description
	    Set objCRb = objWMIService.Get("Win32_NetworkAdapter.DeviceID='"&objCR.Index&"'")
	    WriteXMLAttrTag file,"manufacturer",objCRb.Manufacturer
	    WriteXMLAttrTag file,"mac",objCR.MACAddress
	    ' On récupère un tableau d'adresse IP, il faut donc le parcourir du plus petit index au plus grand...
	    For i=LBound(objCR.IPAddress) To UBound(objCR.IPAddress)
	    	If objCR.IPAddress(i)= "" Then
	    		WriteXMLAttrTag file,"ip","0.0.0.0"
	    	Else
	    		WriteXMLAttrTag file,"ip",objCR.IPAddress(i)
	    	End if
	    Next
	    ' idem IP
	    For i=LBound(objCR.IPSubnet) To UBound(objCR.IPSubnet)
	    	If objCR.IPSubnet(i)= "" Then
	    		WriteXMLAttrTag file,"subnetmask","0.0.0.0"
	    	Else
	    		WriteXMLAttrTag file,"subnetmask",objCR.IPSubnet(i)
	    	End if
	    Next
	    ' idem IP, on fini avec la passerelle
	    For i=LBound(objCR.DefaultIPGateway) To UBound(objCR.DefaultIPGateway)
	    	If objCR.DefaultIPGateway(i)= "" Then
	    		WriteXMLAttrTag file,"gateway","0.0.0.0"
	    	Else
	    		WriteXMLAttrTag file,"gateway",objCR.DefaultIPGateway(i)
	    	End if
	    Next
	    WriteXMLTag file,"/component"
	End If	    
Next
Set colCR = Nothing

' Récupération Cartes graphique
Set colCG = objWMIService.ExecQuery ("Select * from Win32_VideoController")
For each objCG in colCG
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type", "Graphic Card"
    WriteXMLTagContent file,"name",objCG.Description
    WriteXMLAttrTag file,"manufacturer",objCG.AdapterCompatibility
    WriteXMLAttrTag file,"memory",oGetMo(objCG.AdapterRAM)
    WriteXMLTag file,"/component"
Next
Set colCG = Nothing

' Récupération Cartes son
Set colCS = objWMIService.ExecQuery ("Select * from Win32_SoundDevice")
For each objCS in colCS
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type", "Audio Card"
    WriteXMLTagContent file,"name",objCS.Description
    WriteXMLAttrTag file,"manufacturer",objCS.Manufacturer
    WriteXMLTag file,"/component"
Next
Set colCS = Nothing

' Section Périphériques Entré/Sortie
WriteXMLTag file,"!-- Input/Output Peripherals--"
If B_verbose = True or B_vverbose = True Then WScript.Echo "Input/Output Peripherals..."

' Récupération Ecran
Set colS = objWMIService.ExecQuery ("Select * from Win32_DesktopMonitor")
For each objS in colS
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type", "Desktop Monitor"
    WriteXMLTagContent file,"name",objS.Description
    WriteXMLAttrTag file,"manufacturer",objS.MonitorManufacturer
    WriteXMLTag file,"/component"
Next
Set colS = Nothing

' Récupération Clavier
Set colK = objWMIService.ExecQuery ("Select * from Win32_Keyboard")
For each objK in colK
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type", "Keyboard"
    WriteXMLTagContent file,"name",objK.Description
    WriteXMLAttrTag file,"layout",objK.Layout
    WriteXMLTag file,"/component"
Next
Set colK = Nothing

' Récupération Souris
Set colM = objWMIService.ExecQuery ("Select * from Win32_PointingDevice")
For each objM in colM
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type", "Pointing Device"
    WriteXMLTagContent file,"name",objM.Description
    WriteXMLAttrTag file,"manufacturer",objM.Manufacturer
    WriteXMLAttrTag file,"numberofbuttons",objM.NumberOfButtons   
    WriteXMLTag file,"/component"
Next
Set colM = Nothing

' Section Lecteurs
WriteXMLTag file,"!-- Drives --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "Drives..."

' Récupération Lecteurs cd et dvd
Set colCD = objWMIService.ExecQuery ("Select * from Win32_CDROMDrive")
For each objCD in colCD
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","CD/DVD Drive"
    WriteXMLTagContent file,"name",objCD.Caption
    WriteXMLAttrTag file,"manufacturer",objCD.Manufacturer
    WriteXMLAttrTag file,"media",objCD.MediaType
    WriteXMLTag file,"/component"
Next
Set colCD = Nothing

' Récupération Lecteurs disquettes
Set colFD = objWMIService.ExecQuery ("Select * from Win32_FloppyDrive")
For each objFD in colFD
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","Floppy"
    WriteXMLTagContent file,"name",objFD.Caption
    WriteXMLAttrTag file,"manufacturer",objFD.Manufacturer
    WriteXMLTag file,"/component"
Next
Set colFD = Nothing

'Fermeture section lecteurs

' Section controleurs
WriteXMLTag file,"!-- Controllers --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "Controllers..."

' Récupération controlleurs
Set colSC = objWMIService.ExecQuery ("Select * from Win32_SCSIController")
For each objSC in colSC
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","SCSI Controller"
    WriteXMLTagContent file,"name",objSC.Caption
    WriteXMLAttrTag file,"manufacturer",objSC.Manufacturer
    WriteXMLTag file,"/component"
Next
Set colSC = Nothing

Set colIC = objWMIService.ExecQuery ("Select * from Win32_IDEController")
For each objIC in colIC
	WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","IDE Controller"
    WriteXMLTagContent file,"name",objIC.Caption
    WriteXMLAttrTag file,"manufacturer",objIC.Manufacturer
    WriteXMLTag file,"/component"
Next
Set colIC = Nothing

'Fermeture section controleurs

' Section Périphériques USB
If B_usb = True Then

	WriteXMLTag file,"!-- USB Devices --"
	If B_verbose = True or B_vverbose = True Then WScript.Echo "USB Devices..."
	
	' Récupération Périphériques USB
	Set colUSBDevices = objWMIService.ExecQuery ("Select * From Win32_USBControllerDevice")
	for Each objUSBdevice in colUSBDevices
		strDeviceName = objDevice.Dependent
    		strQuotes = Chr(34)
	    	strDeviceName = Replace(strDeviceName, strQuotes, "")
	    	arrDeviceNames = Split(strDeviceName, "=")
	    	strDeviceName = arrDeviceNames(1)
	    	Set colUSBDevices = objWMIService.ExecQuery ("Select * From Win32_PnPEntity Where DeviceID = '" & strDeviceName & "'")
	    	For Each USBDevice in objUSBdevice
       		 	WriteXMLTag file,"component"
    			WriteXMLTagContent file,"type","USB Device"
	    		WriteXMLTagContent file,"name",USBDevice.Name
	    		WriteXMLAttrTag file,"manufacturer",USBDevice.Manufacturer
    			WriteXMLTag file,"/component"
	    	Next    
	Next
	set colUSBDevices = Nothing
End If

'Fermeture Périphériques USB

' Section Imprimantes
WriteXMLTag file,"!-- Printers --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "Printers..."

'Recupération Imprimantes
Set colPrinters = objWMIService.ExecQuery("Select * from Win32_Printer",,48)
For Each objPrinter in colPrinters
	WriteXMLTag file,"component"
	WriteXMLTagContent file,"type","Printer"
	WriteXMLTagContent file,"name",objPrinter.Name
	WriteXMLAttrTag file,"port",objPrinter.PortName
	WriteXMLAttrTag file,"driver",objPrinter.DriverName
	WriteXMLAttrTag file,"default_printer",objPrinter.Default
	WriteXMLAttrTag file,"Location",objPrinter.Location
	WriteXMLAttrTag file,"Local",objPrinter.Local
	WriteXMLAttrTag file,"Shared",objPrinter.Shared
	WriteXMLAttrTag file,"ShareName",objPrinter.ShareName
	WriteXMLTag file,"/component"
Next
set colPrinters = Nothing

'Fermeture Section Imprimantes

' Section OS
WriteXMLTag file,"!-- Operating System --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "Operating System..."

' Récupération Informations OS
Set colSettings = objWMIService.ExecQuery ("Select * from Win32_OperatingSystem")
For Each objOS in colSettings 
    WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","Operating System"
    WriteXMLTagContent file,"name",objOS.Caption
    WriteXMLAttrTag file,"version",objOS.Version
    WriteXMLAttrTag file,"ServicePack",objOS.ServicePackMajorVersion & "." & objOS.ServicePackMinorVersion
    WriteXMLTag file,"/component"
Next
Set colSettings = Nothing

'Fermeture section OS

'#########################################
' Liste une clé dans la BDR
If B_App = True Then
	Dim strKey, strSubKey
	Dim objRegistry
	Dim arrSubKeys()
	Dim strDisplayName, strDisplayVersion, strInstallLocation
	
	strDisplayName = "N/A"
	strDisplayVersion = "N/A"
	strInstallLocation = "N/A"
	Const HKEY_LOCAL_MACHINE = &H80000002
	
	strKey = "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"
	
	WriteXMLTag file,"!-- Application --"
	If B_verbose = True or B_vverbose = True Then WScript.Echo "Application..."
	
	' Récupération Applications
	Set objRegistry = GetObject("winmgmts:{impersonationLevel=Impersonate}!\\"& target & "\root\default:StdRegProv")
	objRegistry.EnumKey HKEY_LOCAL_MACHINE, strKey, arrSubKeys
	
	For Each strSubKey In arrSubKeys
		objRegistry.GetStringValue HKEY_LOCAL_MACHINE,strKey & "\" & strSubKey, "DisplayName", strDisplayName
		objRegistry.GetStringValue HKEY_LOCAL_MACHINE,strKey & "\" & strSubKey, "DisplayVersion", strDisplayVersion
		objRegistry.GetStringValue HKEY_LOCAL_MACHINE,strKey & "\" & strSubKey, "InstallLocation", strInstallLocation
		
		If strDisplayName <> "" Then
		WriteXMLTag file,"component"
    	WriteXMLTagContent file,"type","Application"
    	WriteXMLTagContent file,"name",strDisplayName & " " & strDisplayVersion
    	WriteXMLAttrTag file,"installlocation", ASReplace(strInstallLocation)
    	WriteXMLTag file,"/component"
		End If
		
		strDisplayName = vbEmpty
		strDisplayVersion = vbEmpty
	Next
	set objRegistry = Nothing
End If
'Fermeture section applications
'#########################################

'#########################################
'Section Audit
If B_Audit = True Then
WriteXMLTag file,"!-- AUDIT --"
If B_verbose = True or B_vverbose = True Then WScript.Echo "PARTITIONS..."

' Récupération partitions
Set colPartitions = objWMIService.ExecQuery("Select * From Win32_LogicalDisk where DriveType=3",,48)
For Each objPartition in colPartitions
     WriteXMLTag file,"component"
     WriteXMLTagContent file,"type","Audit"
     WriteXMLTagContent file,"name","Partition " & objPartition.Caption
     WriteXMLAttrTag file,"filesystem",objPartition.FileSystem
     WriteXMLAttrTag file,"size",oGetMo(objPartition.Size)
     WriteXMLAttrTag file,"freespace",oGetMo(objPartition.FreeSpace)
     WriteXMLTag file,"/component"
Next
Set colPartition = Nothing

' Uptime
If B_verbose = True or B_vverbose = True Then WScript.Echo "UPTIME..."
Set colOperatingSystems = objWMIService.ExecQuery ("Select * from Win32_OperatingSystem")
 
For Each objOS in colOperatingSystems
    Bootup = objOS.LastBootUpTime
    LastBootupTime = WMIDateStringToDate(Bootup)
    SystemUptime = DateDiff("s", LastBootUpTime, Now)
    WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","Audit"
    WriteXMLTagContent file,"name","Uptime"
    WriteXMLAttrTag file,"second",SystemUptime
    WriteXMLTag file,"/component"
Next

' Login User
If B_verbose = True or B_vverbose = True Then WScript.Echo "LOGIN USER..."
Set wshNet = WScript.createobject("wscript.Network")
WriteXMLTag file,"component"
WriteXMLTagContent file,"type","Audit"
WriteXMLTagContent file,"name","LoginUser"
WriteXMLAttrTag file,"Name",wshNet.userdomain & "\" & wshNet.username
WriteXMLTag file,"/component"

' ProductID
If B_verbose = True or B_vverbose = True Then WScript.Echo "ProductID..."
Set colOperatingSystems = objWMIService.ExecQuery ("Select * from Win32_OperatingSystem")
 
For Each objOS in colOperatingSystems
    WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","Audit"
    WriteXMLTagContent file,"name","ProductID"
    WriteXMLAttrTag file,"SerialNumber",objOS.SerialNumber
    WriteXMLTag file,"/component"
Next

' ProductKey
If B_verbose = True or B_vverbose = True Then WScript.Echo "ProductKey..."
 
WriteXMLTag file,"component"
WriteXMLTagContent file,"type","Audit"
WriteXMLTagContent file,"name","ProductKey"
WriteXMLAttrTag file,"SerialNumber",ProductKey()
WriteXMLTag file,"/component"

' PageFile
If B_verbose = True or B_vverbose = True Then WScript.Echo "Page File..."
Set colPageFile = objWMIService.ExecQuery ("Select * from Win32_PageFile")
Dim TotalSwapSize
TotalSwapSize=0
 
For Each objPF in colPageFile
    WriteXMLTag file,"component"
    WriteXMLTagContent file,"type","Audit"
    WriteXMLTagContent file,"name","PageFile"
    WriteXMLAttrTag file,"Name",objPF.Name
    WriteXMLAttrTag file,"Size",oGetMo(objPF.FileSize)
    WriteXMLTag file,"/component"
    TotalSwapSize = TotalSwapSize + oGetMo(objPF.FileSize)
Next

' On calcul ici la taille total de la swap sur la machine
WriteXMLTag file,"component"
WriteXMLTagContent file,"type","Audit"
WriteXMLTagContent file,"name","TotalSwapSize"
WriteXMLAttrTag file,"size",TotalSwapSize
WriteXMLTag file,"/component"

End If
'Fermeture Audit
'#########################################

'#########################################
' Section composant SCRIPT
WriteXMLTag file,"!-- Script --"
WriteXMLTag file,"component"
WriteXMLTagContent file,"type","Script"
WriteXMLTagContent file,"name","Inventory"
WriteXMLAttrTag file,"version",VERSION & " sur " & OS
WriteXMLAttrTag file,"BuildDate",BDATE
WriteXMLAttrTag file,"time",DateDiff("s", Datedeb, Now)
WriteXMLAttrTag file,"method", str_Method
WriteXMLAttrTag file,"location", location
WriteXMLTag file,"/component"
' Fermeture section composant SCRIPT

' Fermeture fichier XML
WriteXMLTag file,"/computer"
file.close()

' Upload FTP si nécessaire...
If str_method = "ftp" Then
	If B_verbose = True or B_vverbose = True Then WScript.Echo "Upload FTP..."
	' On temporise le temps que Windows écrive et ferme le fichier...
	WScript.Sleep(5000)
	' Upload du fichier xml 
	Set objShell = CreateObject("WScript.Shell")
	str_Command = "ftp -s:" & str_TempFolderName & "\" & str_TempFileName
	Set objExecObject = objShell.Exec(str_Command)
	' Suppression du fichier de commande ftp et XML
	WScript.Sleep(5000)
	If B_verbose = True or B_vverbose = True Then WScript.Echo "Deleting temporary files..."
	Set ftpcmdfile = fso.GetFile(str_TempFolderName & "\" & str_TempFileName)
	ftpcmdfile.Delete
	Set xmlfile = fso.GetFile(out)
	xmlfile.Delete
End If

' Upload HTTP si nécessaire
If str_method = "http" Then
	If B_verbose = True or B_vverbose = True Then WScript.Echo "Upload HTTP..."
	' On temporise le temps que Windows écrive et ferme le fichier...
	WScript.Sleep(5000)
	' Upload du fichier xml 
	Set objShell = CreateObject("WScript.Shell")
	str_Command = "curl -F file1=@""" & out _
		& """ -F subBtn=OK " & """http://" & server_http & remote_path_http _
		& """ -u " & user_http & ":" & password_http
	Set objExecObject = objShell.Exec(str_Command)
	' Suppression du fichier XML
	WScript.Sleep(5000)
	If B_verbose = True or B_vverbose = True Then WScript.Echo "Deleting temporary files..."
	Set xmlfile = fso.GetFile(out)
	'xmlfile.Delete
End If

If B_verbose = True or B_vverbose = True Then WScript.Echo "[Done]"
Set objWMIService = Nothing
Set fso = Nothing
Set objShell = Nothing
If B_perf = True Then WScript.Echo DateDiff("s", Datedeb, Now) & " sec."
